import axios from "axios";

const SuperAdmin_API_BASE_URL = "https://jsonplaceholder.typicode.com/posts";

class SuperAdminServices {
  getBranches() {
    return axios.get(SuperAdmin_API_BASE_URL);
  }

  createBranch(branch) {
    return axios.post(SuperAdmin_API_BASE_URL, branch);
  }

  getBranchById(branchId) {
    return axios.get(SuperAdmin_API_BASE_URL + "/" + branchId);
  }

  updateBranch(branch, branchId) {
    return axios.put(SuperAdmin_API_BASE_URL + "/" + branchId, branch);
  }

  deleteBranch(branchId) {
    return axios.delete(SuperAdmin_API_BASE_URL + "/" + branchId);
  }
}

export default new SuperAdminServices();
