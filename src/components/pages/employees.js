import React, { memo } from "react";

function employees() {
  return (
    <div>
      <div>
        <main className="content">
          <div className="container-fluid p-0">
            <div className="row">
              <div className="col-12 col-lg-7"></div>
              <div className="col-12 col-lg-3 mb-3">
                <form className="d-inline d-sm-inline-block mb-3">
                  <div className="input-group input-group-navbar">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Search Branches…"
                      aria-label="Search"
                    />
                    <button className="btn" type="button">
                      <i className="align-middle" data-feather="search" />
                    </button>
                  </div>
                </form>
              </div>
              <div className="col-12 col-lg-2 mb-3">
                <button
                  className="btn btn-primary btn-lg btn-block"
                  data-toggle="modal"
                  data-target="#addBranchInformation"
                >
                  <i className="align-middle mr-1" data-feather="plus" />
                  Add Employees
                </button>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-lg-12">
                <div className="card bg-light py-2 py-md-3 border">
                  <div className="card-body ">
                    <h4 className="text-center">Employees Infotmation</h4>
                    <div className="row">
                      <div className="col-12 col-lg-3">
                        <div className="card">
                          <div className="card-header text-center">
                            <h4 className="card-title">Amar Mavinagidad</h4>
                            <hr />
                          </div>
                          <div className="card-body">
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="user"
                            />
                            : Male
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="calendar"
                            />
                            : 25-07-1995
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="mail"
                            />
                            : mail@mail.com <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="phone"
                            />
                            : 1234567890
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="map-pin"
                            />
                            : Bangalore, 509262
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="git-pull-request"
                            />
                            : Sales Manager
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="git-pull-request"
                            />
                            : Account
                            <br />
                          </div>
                          <div className="card-footer text-center">
                            <div className="row">
                              <div className="col-12 col-lg-6 mb-2">
                                <button
                                  type="button"
                                  className="btn btn-primary btn-lg btn-block"
                                  data-toggle="modal"
                                  data-target="#editBranchInformation"
                                >
                                  Edit
                                </button>
                              </div>
                              <div className="col-12 col-lg-6">
                                <button
                                  type="button"
                                  className="btn btn-danger btn-block"
                                  data-toggle="modal"
                                  data-target="#deleteBranchInformation"
                                >
                                  Delete
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-12 col-lg-3">
                        <div className="card">
                          <div className="card-header text-center">
                            <h4 className="card-title">Keshav</h4>
                            <hr />
                          </div>
                          <div className="card-body">
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="user"
                            />
                            : Male
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="calendar"
                            />
                            : 25-07-1995
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="mail"
                            />
                            : mail@mail.com <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="phone"
                            />
                            : 1234567890
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="map-pin"
                            />
                            : Bangalore, 509262
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="git-pull-request"
                            />
                            : Sales Manager
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="git-pull-request"
                            />
                            : Account
                            <br />
                          </div>
                          <div className="card-footer text-center">
                            <div className="row">
                              <div className="col-12 col-lg-6 mb-2">
                                <button
                                  type="button"
                                  className="btn btn-primary btn-lg btn-block"
                                  data-toggle="modal"
                                  data-target="#editBranchInformation"
                                >
                                  Edit
                                </button>
                              </div>
                              <div className="col-12 col-lg-6">
                                <button
                                  type="button"
                                  className="btn btn-danger btn-block"
                                  data-toggle="modal"
                                  data-target="#deleteBranchInformation"
                                >
                                  Delete
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-12 col-lg-3">
                        <div className="card">
                          <div className="card-header text-center">
                            <h4 className="card-title">Abhilash</h4>
                            <hr />
                          </div>
                          <div className="card-body">
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="user"
                            />
                            : Male
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="calendar"
                            />
                            : 25-07-1995
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="mail"
                            />
                            : mail@mail.com <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="phone"
                            />
                            : 1234567890
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="map-pin"
                            />
                            : Bangalore, 509262
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="git-pull-request"
                            />
                            : Sales Manager
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="git-pull-request"
                            />
                            : Account
                            <br />
                          </div>
                          <div className="card-footer text-center">
                            <div className="row">
                              <div className="col-12 col-lg-6 mb-2">
                                <button
                                  type="button"
                                  className="btn btn-primary btn-lg btn-block"
                                  data-toggle="modal"
                                  data-target="#editBranchInformation"
                                >
                                  Edit
                                </button>
                              </div>
                              <div className="col-12 col-lg-6">
                                <button
                                  type="button"
                                  className="btn btn-danger btn-block"
                                  data-toggle="modal"
                                  data-target="#deleteBranchInformation"
                                >
                                  Delete
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-12 col-lg-3">
                        <div className="card">
                          <div className="card-header text-center">
                            <h4 className="card-title">Vikas Kumar</h4>
                            <hr />
                          </div>
                          <div className="card-body">
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="user"
                            />
                            : Male
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="calendar"
                            />
                            : 25-07-1995
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="mail"
                            />
                            : mail@mail.com <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="phone"
                            />
                            : 1234567890
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="map-pin"
                            />
                            : Bangalore, 509262
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="git-pull-request"
                            />
                            : Sales Manager
                            <br />
                            <i
                              className="align-middle mr-1 mb-2"
                              data-feather="git-pull-request"
                            />
                            : Account
                            <br />
                          </div>
                          <div className="card-footer text-center">
                            <div className="row">
                              <div className="col-12 col-lg-6 mb-2">
                                <button
                                  type="button"
                                  className="btn btn-primary btn-lg btn-block"
                                  data-toggle="modal"
                                  data-target="#editBranchInformation"
                                >
                                  Edit
                                </button>
                              </div>
                              <div className="col-12 col-lg-6">
                                <button
                                  type="button"
                                  className="btn btn-danger btn-block"
                                  data-toggle="modal"
                                  data-target="#deleteBranchInformation"
                                >
                                  Delete
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <div
          className="modal fade"
          id="addBranchInformation"
          tabIndex={-1}
          role="dialog"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Add Branch Information</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">First Name</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i className="align-middle mr-1" data-feather="user" />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="First Name"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Last Name</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i className="align-middle mr-1" data-feather="user" />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Last Name"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">DOB</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i
                          className="align-middle mr-1"
                          data-feather="calendar"
                        />
                      </div>
                      <input
                        type="date"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Date Of Birth"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <span className="form-check-label">Gender</span>
                    <label className="form-check form-check-inline">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="inline-radios-example"
                        defaultValue="option1"
                      />
                      <span className="form-check-label">Male</span>
                    </label>
                    <label className="form-check form-check-inline">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="inline-radios-example"
                        defaultValue="option1"
                      />
                      <span className="form-check-label">Female</span>
                    </label>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Email</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i className="align-middle mr-1" data-feather="mail" />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Email"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Phone Number</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i className="align-middle mr-1" data-feather="phone" />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Phone Number"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i
                          className="align-middle mr-1"
                          data-feather="map-pin"
                        />
                      </div>
                      <textarea
                        className="form-control"
                        rows={1}
                        placeholder="Address"
                        defaultValue={""}
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Pincode</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i
                          className="align-middle mr-1"
                          data-feather="map-pin"
                        />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Pincode"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Role</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i
                          className="align-middle mr-1"
                          data-feather="git-pull-request"
                        />
                      </div>
                      <select className="form-control">
                        <option />
                        <option>Branch Manager</option>
                        <option>Sales Manager</option>
                        <option>Team</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Department</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i
                          className="align-middle mr-1"
                          data-feather="git-pull-request"
                        />
                      </div>
                      <select className="form-control" placeholder="Department">
                        <option />
                        <option>Account</option>
                        <option>HR</option>
                        <option>Cleaner</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-danger"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" className="btn btn-primary">
                  Add Employee
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="modal fade"
          id="editBranchInformation"
          tabIndex={-1}
          role="dialog"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Edit Employee Information</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">First Name</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i className="align-middle mr-1" data-feather="user" />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="First Name"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Last Name</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i className="align-middle mr-1" data-feather="user" />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Last Name"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">DOB</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i
                          className="align-middle mr-1"
                          data-feather="calendar"
                        />
                      </div>
                      <input
                        type="date"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Date Of Birth"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <span className="form-check-label">Gender</span>
                    <label className="form-check form-check-inline">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="inline-radios-example"
                        defaultValue="option1"
                      />
                      <span className="form-check-label">Male</span>
                    </label>
                    <label className="form-check form-check-inline">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="inline-radios-example"
                        defaultValue="option1"
                      />
                      <span className="form-check-label">Female</span>
                    </label>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Email</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i className="align-middle mr-1" data-feather="mail" />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Email"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Phone Number</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i className="align-middle mr-1" data-feather="phone" />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Phone Number"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i
                          className="align-middle mr-1"
                          data-feather="map-pin"
                        />
                      </div>
                      <textarea
                        className="form-control"
                        rows={1}
                        placeholder="Address"
                        defaultValue={""}
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Pincode</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i
                          className="align-middle mr-1"
                          data-feather="map-pin"
                        />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Pincode"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Role</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i
                          className="align-middle mr-1"
                          data-feather="git-pull-request"
                        />
                      </div>
                      <select className="form-control">
                        <option />
                        <option>Branch Manager</option>
                        <option>Sales Manager</option>
                        <option>Team</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Department</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i
                          className="align-middle mr-1"
                          data-feather="git-pull-request"
                        />
                      </div>
                      <select className="form-control" placeholder="Department">
                        <option />
                        <option>Account</option>
                        <option>HR</option>
                        <option>Cleaner</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-danger"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" className="btn btn-primary">
                  Update Employee
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="modal fade"
          id="deleteBranchInformation"
          tabIndex={-1}
          role="dialog"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Delete Employee Information</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <h4>Do you want delete Employee Information??..</h4>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-primary"
                  data-dismiss="modal"
                >
                  Yes
                </button>
                <button type="button" className="btn btn-danger">
                  No
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default memo(employees);
