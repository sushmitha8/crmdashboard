import axios from "axios";
import React, { memo } from "react";
import { Component } from "react";
import superAdminServices, {
  SuperAdminServices,
} from "../../services/superAdminServices";

class Module1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      branchName: "",
      addressLine1: "",
      phoneNumber: "",
      emailId: "",
      manager: "",
      pincode: "",
    };
    this.state = {
      data: [],
    };
  }

  changeHandler = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  //Save the Branch Information
  saveHandler = (e) => {
    e.preventDefault();
    console.log(this.state);
    superAdminServices
      .createBranch(this.state)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
    this.setState({
      branchName: "",
      addressLine1: "",
      phoneNumber: "",
      emailId: "",
      manager: "",
      pincode: "",
    });
  };

  //Update the Branch Information
  updateHandler() {
    let branch = {
      branchName: this.state.branchName,
      addressLine1: this.state.addressLine1,
      phoneNumber: this.state.phoneNumber,
      emailId: this.state.emailId,
      manager: this.state.manager,
      pincode: this.state.pincode,
    };
    superAdminServices.updateBranch(branch, this.state.id).then((res) => {
      this.props.history.push("/branch");
    });
  }

  //Get all the branch Information
  getData() {
    superAdminServices.getBranches().then((res) => {
      var data = res.data;
      this.setState({ data: data });
    });
  }

  componentDidMount() {
    this.getData();
  }

  //Delete branch based on branchID
  deleteBranch() {
    superAdminServices.deleteBranch(this.state.id);
  }

  render() {
    const { branchName, addressLine1, phoneNumber, emailId, manager, pincode } =
      this.state;

    return (
      <div>
        <main className="content">
          <div className="container-fluid p-0">
            <div className="row">
              <div className="col-12 col-lg-7"></div>
              <div className="col-12 col-lg-3 mb-3">
                <form className="d-inline d-sm-inline-block mb-3">
                  <div className="input-group input-group-navbar">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Search Branches..."
                      aria-label="Search"
                    />
                    <button className="btn" type="button">
                      <i className="align-middle" data-feather="search" />
                    </button>
                  </div>
                </form>
              </div>
              <div className="col-12 col-lg-2 mb-3">
                <button
                  className="btn btn-primary btn-lg btn-block"
                  data-toggle="modal"
                  data-target="#addBranchInformation"
                >
                  <i className="align-middle mr-1" data-feather="plus" />
                  Add Branches
                </button>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-lg-12">
                <div className="card bg-light py-2 py-md-3 border">
                  <div className="card-body ">
                    <h4 className="text-center">Branch Infotmation</h4>
                    <div className="row">
                      {this.state.data.map((d) => (
                        <div className="col-12 col-lg-3">
                          <div className="card" key={d.id}>
                            <div className="card-header text-center">
                              <h4 className="card-title">{d.title}</h4>
                              <hr />
                            </div>
                            <div className="card-body">
                              <i
                                className="align-middle mr-1 mb-2"
                                data-feather="map-pin"
                              />
                              : {d.id} <br />
                              <i
                                className="align-middle mr-1 mb-2"
                                data-feather="phone"
                              />
                              : {d.userId}
                              <br />
                              <i
                                className="align-middle mr-1 mb-2"
                                data-feather="mail"
                              />
                              : {d.userId}
                              <br />
                              <i
                                className="align-middle mr-1 mb-2"
                                data-feather="user"
                              />
                              : {d.userId}
                              <br />
                              <i
                                className="align-middle mr-1 mb-2"
                                data-feather="map-pin"
                              />
                              : {d.userId}
                              <br />
                            </div>
                            <div className="card-footer text-center">
                              <div className="row">
                                <div className="col-12 col-lg-6 mb-2">
                                  <button
                                    type="button"
                                    className="btn btn-primary btn-lg btn-block"
                                    data-toggle="modal"
                                    data-target="#editBranchInformation"
                                  >
                                    Edit
                                  </button>
                                </div>
                                <div className="col-12 col-lg-6">
                                  <button
                                    type="button"
                                    className="btn btn-danger btn-block"
                                    data-toggle="modal"
                                    data-target="#deleteBranchInformation"
                                  >
                                    Delete
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <div
          className="modal fade"
          id="addBranchInformation"
          tabIndex={-1}
          role="dialog"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <form onSubmit={this.saveHandler}>
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title">Add Branch Information</h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modal-body">
                  <div className="row">
                    <div className="col-12 col-lg-6">
                      <label className="sr-only">Branch Name</label>
                      <div className="input-group mb-2 mr-sm-2">
                        <div className="input-group-text">
                          <i
                            className="align-middle mr-1"
                            data-feather="git-branch"
                          />
                        </div>
                        <input
                          type="text"
                          className="form-control"
                          name="branchName"
                          placeholder="Branch Name"
                          value={this.state.branchName}
                          onChange={this.changeHandler}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-lg-6">
                      <div className="input-group mb-2 mr-sm-2">
                        <div className="input-group-text">
                          <i
                            className="align-middle mr-1"
                            data-feather="map-pin"
                          />
                        </div>
                        <textarea
                          className="form-control"
                          rows={2}
                          placeholder="Branch Address"
                          defaultValue={""}
                          name="addressLine1"
                          value={this.state.addressLine1}
                          onChange={this.changeHandler}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-lg-6">
                      <label className="sr-only">Phone Number</label>
                      <div className="input-group mb-2 mr-sm-2">
                        <div className="input-group-text">
                          <i
                            className="align-middle mr-1"
                            data-feather="phone"
                          />
                        </div>
                        <input
                          type="text"
                          className="form-control"
                          id="inlineFormInputGroupUsername2"
                          placeholder="Phone Number"
                          value={this.state.phoneNumber}
                          name="phoneNumber"
                          onChange={this.changeHandler}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-lg-6">
                      <label className="sr-only">Email</label>
                      <div className="input-group mb-2 mr-sm-2">
                        <div className="input-group-text">
                          <i
                            className="align-middle mr-1"
                            data-feather="mail"
                          />
                        </div>
                        <input
                          type="text"
                          className="form-control"
                          id="inlineFormInputGroupUsername2"
                          placeholder="Email"
                          name="emailId"
                          value={this.state.emailId}
                          onChange={this.changeHandler}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-lg-6">
                      <label className="sr-only">Manager</label>
                      <div className="input-group mb-2 mr-sm-2">
                        <div className="input-group-text">
                          <i
                            className="align-middle mr-1"
                            data-feather="user"
                          />
                        </div>
                        <input
                          type="text"
                          className="form-control"
                          id="inlineFormInputGroupUsername2"
                          placeholder="Manager"
                          value={this.state.manager}
                          name="manager"
                          onChange={this.changeHandler}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-lg-6">
                      <label className="sr-only">Pincode</label>
                      <div className="input-group mb-2 mr-sm-2">
                        <div className="input-group-text">
                          <i
                            className="align-middle mr-1"
                            data-feather="map-pin"
                          />
                        </div>
                        <input
                          type="text"
                          className="form-control"
                          id="inlineFormInputGroupUsername2"
                          placeholder="Pincode"
                          value={this.state.pincode}
                          name="pincode"
                          onChange={this.changeHandler}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-danger"
                    data-dismiss="modal"
                  >
                    Close
                  </button>
                  <button type="submit" className="btn btn-primary">
                    Add Branch
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div
          className="modal fade"
          id="editBranchInformation"
          tabIndex={-1}
          role="dialog"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Edit Branch Information</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Branch Name</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i
                          className="align-middle mr-1"
                          data-feather="git-branch"
                        />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Branch Name"
                        value={this.state.branchName}
                        onChange={this.changeHandler}
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i
                          className="align-middle mr-1"
                          data-feather="map-pin"
                        />
                      </div>
                      <textarea
                        className="form-control"
                        rows={2}
                        placeholder="Branch Address"
                        defaultValue={""}
                        value={this.state.addressLine1}
                        onChange={this.changeHandler}
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Phone Number</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i className="align-middle mr-1" data-feather="phone" />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Phone Number"
                        value={this.state.phoneNumber}
                        onChange={this.changeHandler}
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Email</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i className="align-middle mr-1" data-feather="mail" />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Email"
                        value={this.state.emailId}
                        onChange={this.changeHandler}
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Manager</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i className="align-middle mr-1" data-feather="user" />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Manager"
                        value={this.state.manager}
                        onChange={this.changeHandler}
                      />
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <label className="sr-only">Pincode</label>
                    <div className="input-group mb-2 mr-sm-2">
                      <div className="input-group-text">
                        <i
                          className="align-middle mr-1"
                          data-feather="map-pin"
                        />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        id="inlineFormInputGroupUsername2"
                        placeholder="Pincode"
                        value={this.state.pincode}
                        onChange={this.changeHandler}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-danger"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button
                  type="submit"
                  className="btn btn-primary"
                  onClick={this.updateHandler}
                >
                  Update Branch
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="modal fade"
          id="deleteBranchInformation"
          tabIndex={-1}
          role="dialog"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Delete Branch Information</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <h4>Do you want delete Branch Information??..</h4>
              </div>
              <div className="modal-footer">
                <button
                  type="submit"
                  className="btn btn-primary"
                  onClick={this.deleteBranch}
                >
                  Yes
                </button>
                <button
                  type="button"
                  className="btn btn-danger"
                  data-dismiss="modal"
                >
                  No
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default memo(Module1);
