import React, { memo } from "react";
import { Link } from "react-router-dom";

const navOptions = [
  { title: "Login", path: "/login" },
  { title: "Register", path: "/register" },
];

function LandingPage() {
  return (
    <div class="container h-100 d-flex justify-content-center">
      <div class="jumbotron my-auto">
        <Link to="/login">Back to login</Link>
      </div>
    </div>
  );
}

export default memo(LandingPage);
