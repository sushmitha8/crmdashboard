export { default as LandingPage } from "./LandingPage";
export { default as Login } from "./Login";
export { default as Register } from "./Register";
export { default as ForgotPassword } from "./ForgotPassword";
export { default as Module1 } from "./Module1";
export { default as Dashboard } from "./Dashboard";
export { default as employees } from "./employees";
