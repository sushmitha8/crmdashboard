import React, { memo } from "react";
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";

// import { Link } from "react-router-dom";
// import { isLoggedIn } from "utils";
function SideBar(props) {
  let history = useHistory();

  function handleLogout() {
    localStorage.removeItem("roles");
    history.push("/");
  }

  return (
    <nav id="sidebar" className="sidebar">
      <div className="sidebar-content js-simplebar">
        <a className="sidebar-brand" href="dashboard.html">
          <span className="align-middle">Hastar CRM</span>
        </a>
        <ul className="sidebar-nav">
          <li className="sidebar-header" />
          <li className="sidebar-item active">
            <a className="sidebar-link" href="dashboard.html">
              <i className="align-middle" data-feather="sliders" />
              <span className="align-middle">Dashboard</span>
            </a>
          </li>
          <li className="sidebar-item">
            <a className="sidebar-link" href="branch.html">
              <i className="align-middle" data-feather="briefcase" />
              <span className="align-middle">Branch/Account</span>
            </a>
          </li>
          <li className="sidebar-item">
            <a className="sidebar-link" href="#">
              <i className="align-middle" data-feather="users" />
              <span className="align-middle">Employees</span>
            </a>
          </li>
          <li className="sidebar-item">
            <a className="sidebar-link" href="#">
              <i className="align-middle" data-feather="archive" />
              <span className="align-middle">Vehicles</span>
            </a>
          </li>
        </ul>
        <ul class="nav nav-pills flex-column logout-button">
          <li class="sidebar-item">
            <a class="sidebar-link" href="#">
              <i class="align-middle" data-feather="settings"></i>
              <span class="align-middle">Settings</span>
            </a>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link" href="#">
              <i class="align-middle" data-feather="help-circle"></i>
              <span class="align-middle">Help</span>
            </a>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link" href="#">
              <i class="align-middle" data-feather="log-out"></i>
              <span class="align-middle">Logout</span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
}

SideBar.propTypes = {
  routes: PropTypes.arrayOf(
    PropTypes.shape({
      path: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
    })
  ).isRequired,
  prefix: PropTypes.string,
  className: PropTypes.string,
};

SideBar.defaultProps = {
  prefix: "",
  className: "",
};

export default memo(SideBar);
