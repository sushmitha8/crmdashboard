import React from "react";

function Header(props) {
  return (
    <nav className="navbar navbar-expand navbar-light navbar-bg">
      <a className="sidebar-toggle d-flex mr-5">
        <i className="hamburger align-self-center" />
      </a>
      <div className="col-auto d-none d-sm-block mr-user-center">
        <h1>
          <strong>Hello, Jamet R </strong>
        </h1>
        <span>November 24, 2021</span>
      </div>
      <form
        className="d-inline d-sm-inline-block"
        style={{ margin: "auto", width: 600 }}
      >
        <div className="input-group input-group-navbar">
          <input
            type="text"
            className="form-control"
            placeholder="Search.."
            aria-label="Search"
          />
          <button className="btn" type="button">
            <i className="align-middle" data-feather="search" />
          </button>
        </div>
      </form>
      <div className="navbar-collapse collapse">
        <ul className="navbar-nav navbar-align">
          <li className="nav-item dropdown">
            <a
              className="nav-icon dropdown-toggle"
              href="#"
              id="alertsDropdown"
              data-toggle="dropdown"
            >
              <div className="position-relative">
                <i className="align-middle" data-feather="bell" />
                <span className="indicator">4</span>
              </div>
            </a>
            <div
              className="
      dropdown-menu dropdown-menu-lg dropdown-menu-right
      py-0
    "
              aria-labelledby="alertsDropdown"
            >
              <div className="dropdown-menu-header">4 New Notifications</div>
              <div className="list-group">
                <a href="#" className="list-group-item">
                  <div className="row g-0 align-items-center">
                    <div className="col-2">
                      <img
                        src={
                          process.env.PUBLIC_URL +
                          "/assets/img/avatars/avatar-5.jpg"
                        }
                        className="avatar img-fluid rounded-circle mr-4"
                      />
                    </div>
                    <div className="col-10">
                      <div className="text-dark">Update completed</div>
                      <div className="text-muted small mt-1">
                        Restart server 12 to complete the update.
                      </div>
                      <div className="text-muted small mt-1">30m ago</div>
                    </div>
                  </div>
                </a>
                <a href="#" className="list-group-item">
                  <div className="row g-0 align-items-center">
                    <div className="col-2">
                      <img
                        src={
                          process.env.PUBLIC_URL +
                          "/assets/img/avatars/avatar-2.jpg"
                        }
                        className="avatar img-fluid rounded-circle mr-4"
                      />
                    </div>
                    <div className="col-10">
                      <div className="text-dark">Lorem ipsum</div>
                      <div className="text-muted small mt-1">
                        Aliquam ex eros, imperdiet vulputate hendrerit et.
                      </div>
                      <div className="text-muted small mt-1">2h ago</div>
                    </div>
                  </div>
                </a>
                <a href="#" className="list-group-item">
                  <div className="row g-0 align-items-center">
                    <div className="col-2">
                      <img
                        src={
                          process.env.PUBLIC_URL +
                          "/assets/img/avatars/avatar-3.jpg"
                        }
                        className="avatar img-fluid rounded-circle mr-4"
                      />
                    </div>
                    <div className="col-10">
                      <div className="text-dark">Login from 192.186.1.8</div>
                      <div className="text-muted small mt-1">5h ago</div>
                    </div>
                  </div>
                </a>
                <a href="#" className="list-group-item">
                  <div className="row g-0 align-items-center">
                    <div className="col-2">
                      <img
                        src={
                          process.env.PUBLIC_URL +
                          "/assets/img/avatars/avatar-4.jpg"
                        }
                        className="avatar img-fluid rounded-circle mr-4"
                      />
                    </div>
                    <div className="col-10">
                      <div className="text-dark">New connection</div>
                      <div className="text-muted small mt-1">
                        Christina accepted your request.
                      </div>
                      <div className="text-muted small mt-1">14h ago</div>
                    </div>
                  </div>
                </a>
              </div>
              <div className="dropdown-menu-footer">
                <a href="#" className="text-muted">
                  Show all notifications
                </a>
              </div>
            </div>
          </li>
          <li className="nav-item dropdown">
            <a
              className="nav-icon dropdown-toggle d-inline-block d-sm-none"
              href="#"
              data-toggle="dropdown"
            >
              <img
                src={process.env.PUBLIC_URL + "/assets/img/avatars/avatar.jpg"}
                className="avatar img-fluid rounded-circle mr-1"
              />
            </a>
            <a
              className="nav-link dropdown-toggle d-none d-sm-inline-block"
              href="#"
              data-toggle="dropdown"
            >
              <img
                src={process.env.PUBLIC_URL + "/assets/img/avatars/avatar.jpg"}
                className="avatar img-fluid rounded-circle mr-1"
              />
              <span className="text-dark">Jamet R</span>
            </a>
            <div className="dropdown-menu dropdown-menu-right">
              <a className="dropdown-item" href="#">
                <i className="align-middle mr-1" data-feather="user" />
                Profile
              </a>
              <div className="dropdown-divider" />
              <a className="dropdown-item" href="#">
                <i className="align-middle mr-1" data-feather="log-out" />
                Log out
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Header;
