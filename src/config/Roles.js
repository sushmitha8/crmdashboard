/*
 * These are the placeholder roles you can replace these with yours
 */
export default {
  SUPER_ADMIN: "SUPER_ADMIN",
  ADMIN: "ADMIN",
  BRANCH_MANAGER: "BRANCH_MANAGER",
  TEAM_LEADER: "TEAM_LEADER",
  SALES_EXECUTIVE: "SALES_EXECUTIVE",
};
