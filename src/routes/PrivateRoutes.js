import React, { Fragment } from "react";
import { Redirect, useRouteMatch } from "react-router-dom";
import { getAllowedRoutes, isLoggedIn } from "utils";
import { PrivateRoutesConfig } from "config";
import { SideBar, Header } from "components/common";
import MapAllowedRoutes from "routes/MapAllowedRoutes";

function PrivateRoutes() {
  const match = useRouteMatch("/crm");
  let allowedRoutes = [];

  if (isLoggedIn()) allowedRoutes = getAllowedRoutes(PrivateRoutesConfig);
  else return <Redirect to="/" />;

  return (
    <Fragment>
      <SideBar
        routes={allowedRoutes}
        prefix={match.path}
        className="bg-white"
      />
      <div className="main">
        <Header />
        <MapAllowedRoutes
          routes={allowedRoutes}
          basePath="/crm"
          isAddNotFound
        />
      </div>
    </Fragment>
  );
}

export default PrivateRoutes;
